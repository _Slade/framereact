import firebase from '../firebase';

type QuerySnapshot = firebase.firestore.QuerySnapshot;

function getDataFromQuerySnapshot<T>(snapshot: Promise<QuerySnapshot>): Promise<T[]> {
  return snapshot.then(snapshot => snapshot.docs.map(doc => doc.data() as T));
}

export function getCollection<T>(name: string): Promise<T[]> {
  const db = firebase.firestore();
  const collection = db.collection(name);
  return getDataFromQuerySnapshot(collection.get());
}
