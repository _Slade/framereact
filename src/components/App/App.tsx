import * as React from 'react';

import './App.css';
import Home from '../Home/Home';
import logo from '../../assets/logo.svg';

class App extends React.Component {
  public render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to FrameReact</h1>
        </header>
        <section className="App-body">
          <Home />
        </section>
      </div>
    );
  }
}

export default App;
