import * as React from 'react';
import Card from '@material-ui/core/Paper';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import CircularProgress from '@material-ui/core/CircularProgress';

import './Home.css';
import * as db from '../../services/database';
import { Game } from '../../types/wiki';

class Home extends React.Component {
  state: { games: Game[] | undefined };

  constructor(props: any) {
    super(props);
    this.state = { games: undefined };
  }

  componentDidMount() {
    console.log('Fetching games');
    db.getCollection<Game>('games').then((games: Game[]) => {
      console.log('Got games');
      this.setState({ games });
    });
  }

  public render() {
    return (
      <Card>
        <CardHeader> Games </CardHeader>
        <CardContent>
          { this.state.games == null
            ? <CircularProgress />
            : <div>
                <ul>
                  {this.state.games.map(game => <li key={game.slug}>{game.name}</li>)}
                </ul>
              </div>
          }
        </CardContent>
      </Card>
    );
  }
}

export default Home;
