import * as firebase from 'firebase/app';
import 'firebase/firestore';

const config = {
  apiKey: "AIzaSyC-rJ--IitDJkWPh0xbNPRokXOV4fh17Kg",
  authDomain: "framereact-f2c97.firebaseapp.com",
  databaseURL: "https://framereact-f2c97.firebaseio.com",
  projectId: "framereact-f2c97",
  storageBucket: "framereact-f2c97.appspot.com",
  messagingSenderId: "302548366557"
};

firebase.initializeApp(config);

firebase.firestore().settings({
  timestampsInSnapshots: true,
});

export default firebase;
