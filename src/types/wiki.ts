export interface Game {
  id: string;
  slug: string;
  name: string;
}

export interface Character {
  id: string;
  game: Game;
  slug: string;
  name: string;
}
