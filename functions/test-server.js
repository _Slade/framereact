const app = require('express')();
const morgan = require('morgan');
const path = require('path');

const PORT = 1234;

app.use(morgan('tiny'));

let count = 0;

app.get('/:char-frame-data-:game/json', (req, res) => {
  if (req.params.char === 'edge-master') {
    res.status(404).send('<p>error</p>');
  }
  else {
    const row = {
      type: 'fd1row', atk: count++, cmd: 'DUMMY', lvl: 'DUMMY', dmg: 'DUMMY',
      imp: 'DUMMY', grd: 'DUMMY', hit: 'DUMMY', cnt: 'DUMMY', nts: 'DUMMY',
    };
    res.send(new Array(100).fill(row));
  }
});
app.get('/:game/json', (req, res) => {
  res.sendFile(path.join(__dirname, 'test_responses', req.params.game + '.json'));
});

console.log('Listening on port', PORT);
app.listen(PORT);
