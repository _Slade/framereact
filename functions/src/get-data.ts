import * as admin from 'firebase-admin';
import * as sleep from 'sleep-promise';
import {
  DocumentReference,
  Firestore,
  QuerySnapshot,
  WriteBatch,
  WriteResult
} from '@google-cloud/firestore';

import * as wikiApi from './wiki-api';
import * as constants from './constants';
import { Game } from './wiki-types';

type PromiseFactory<T> = () => PromiseLike<T>;

export async function getData(
  db: Firestore,
  apiService: wikiApi.WikiApiService,
  throttle: number,
): Promise<void> {
  console.log('Getting games');
  const games: Game[] = await db.collection(constants.collections.GAMES)
    .orderBy('slug', 'desc')
    .get()
    .then(convertGames);
  console.log(`Got ${games.length} games`);
  for (const game of games) {
    game.characters = await apiService.getCharacters(game.slug);
  }
  const getMoveCallbacks = [];
  for (const game of games) {
    if (game.characters != null) {
      for (const character of game.characters) {
        getMoveCallbacks.push(async () => {
          console.log(`Getting moves for ${character.name} in ${game.name}`);
          apiService.getMoves(character, game.frameSlug).then(moves => {
            console.log('Got', moves.length, 'moves');
            character.moves = moves;
          }).catch(err => {
            console.log('Error getting moves:', err);
            character.moves = [];
          });
        });
      }
    }
  }
  // TODO Empty collections before adding new data, or use update logic instead
  executeThrottled(getMoveCallbacks, throttle).then(async () => {
    console.log('Got', games.length, 'games');
    const batch = new BatchWrapper(db);
    for (const game of games) {
      const gameDoc = db.collection(constants.collections.FRAME_DATA).doc(game.slug);
      await batch.set(gameDoc, {
        slug: game.slug,
        name: game.name,
        frameSlug: game.frameSlug,
      });
      if (game.characters != null) {
        for (const character of game.characters) {
          const charDoc = gameDoc.collection(constants.collections.CHARACTERS).doc(character.slug);
          await batch.set(charDoc, {
            name: character.name,
            slug: character.slug,
          });
          if (character.moves != null) {
            for (let i = 0; i < character.moves.length; i++) {
              const move = character.moves[i];
              await batch.set(charDoc.collection(constants.collections.MOVES).doc(`${i + 1}`), move);
            }
          }
        }
      }
    }
    await batch.commit();
  });
}

class BatchWrapper {
  private db: Firestore;
  private _batchCount = 0;
  private batch: WriteBatch;
  private _writeResults: WriteResult[][] = [];

  constructor(db: Firestore) {
    this.db = db;
    this.batch = db.batch();
  }

  async set(doc: DocumentReference, val: any): Promise<void> {
    // Commit the current batch and get a new one
    if (this._batchCount >= 500) {
      await this.commit();
    }
    this.batch.set(doc, val);
    this._batchCount++;
  }

  async commit(): Promise<void> {
    this._writeResults.push(await this.batch.commit());
    this.batch = this.db.batch();
    this._batchCount = 0;
  }

  get writeResults(): WriteResult[][] {
    return this._writeResults;
  }
}

function convertGames(games: QuerySnapshot): Game[] {
  return games.docs.map((doc: admin.firestore.DocumentSnapshot) =>
    doc.data() as Game
  );
}

function executeThrottled<T>(tasks: PromiseFactory<T>[], delayInMs: number): Promise<T[]> {
  return tasks.reduce((acc: Promise<T[]>, cur: PromiseFactory<T>) => {
    return acc.then(values => {
      const promise: PromiseLike<T> = cur();
      return promise.then(sleep(delayInMs)).then(value => {
        return [ ...values, value ];
      });
    });
  }, Promise.resolve([]));
}
