export const
  TEST_WIKI_URL = 'http://localhost:1234',
  PROD_WIKI_URL = 'https://8wayrun.com/wiki',
  TEST_THROTTLE = 0,
  PROD_THROTTLE = 3000;

export namespace collections {
  export const
    GAMES = 'games',
    FRAME_DATA = 'frameData',
    CHARACTERS = 'characters',
    MOVES = 'moves';
}

