export interface Game {
  name: string;
  slug: string;
  frameSlug: string;
  characters?: Character[];
}

export interface Character {
  name: string;
  slug: string;
  moves?: Move[];
}

export interface Move {
  type: 'fd0row' | 'fd1row' | 'fd4row' | 'fd5row' | 'fd6row';
  atk: string;
  cmd: string;
  lvl: string;
  dmg: string;
  imp: string;
  grd: string;
  hit: string;
  cnt: string;
  nts: string;
}

export interface MoveSBE extends Move {
  type: 'fd0row';
  wb: string;
}

export interface MoveSC1 extends Move {
  type: 'fd1row';
}

export interface MoveSC4 extends Move {
  type: 'fd4row';
  scr: string;
}

export interface MoveSC5 extends Move {
  type: 'fd5row';
  jg: string;
  gb: string;
}

export interface MoveSC6 extends Move {
  type: 'fd6row';
  chip: string;
  gb: string;
}
