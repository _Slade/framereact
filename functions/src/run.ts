import * as admin from 'firebase-admin';

import * as constants from './constants';
import * as wikiApi from './wiki-api';
import { getData } from './get-data';

function main(): void {
  admin.initializeApp({
    credential: admin.credential.cert(require('../key.json'))
  });
  admin.firestore().settings({
    timestampsInSnapshots: true,
  });

  getData(
    admin.firestore(),
    new wikiApi.WikiApiService(constants.PROD_WIKI_URL),
    constants.PROD_THROTTLE,
  );
}

main();
