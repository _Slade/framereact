import {
  CollectionReference,
  DocumentReference,
  Firestore,
  Query,
  QueryDocumentSnapshot,
  QuerySnapshot,
  WriteBatch,
  WriteResult
} from '@google-cloud/firestore';
import { anyString, anything, instance, mock, when } from 'ts-mockito';

import * as constants from './constants';
import * as wikiApi from './wiki-api';
import { Game } from './wiki-types';
import { getData } from './get-data';

function main(): void {
  const apiService = new wikiApi.WikiApiService(constants.TEST_WIKI_URL);
  console.log('Created API service');
  const db: Firestore = mock(Firestore);
  console.log('Mocked database');

  // Mock collections
  when(db.collection(anyString())).thenCall((coll: string) => {
    console.log(`Returning mock collection "${coll}"`);
    if (coll === constants.collections.GAMES) {
      return instance(mockGamesCollection());
    }
    return instance(mockCollection());
  });

  // Mock write batches
  when(db.batch()).thenCall(newMockWriteBatch);

  console.log('Getting data');
  getData(instance(db), apiService, constants.TEST_THROTTLE).then(() => {
    console.log('Finished getting data');
  }).catch(err => {
    console.log('Failed to get data:', err);
  });
}

function mockGamesCollection(): CollectionReference {
  const mockQuerySnapshot: QuerySnapshot = {
    docs: [
      { slug: 'soulblade',    name: 'Soul Blade',      frameSlug: 'sbe' },
      { slug: 'soulcalibur' , name: 'Soulcalibur',     frameSlug: 'sc1' },
      { slug: 'soulcalibur2', name: 'Soulcalibur II',  frameSlug: 'sc2' },
      { slug: 'soulcalibur3', name: 'Soulcalibur III', frameSlug: 'sc3' },
      { slug: 'soulcalibur4', name: 'Soulcalibur IV',  frameSlug: 'sc4' },
      { slug: 'soulcalibur5', name: 'Soulcalibur V',   frameSlug: 'sc5' },
      { slug: 'soulcalibur6', name: 'Soulcalibur VI',  frameSlug: 'sc6' },
    ].map(data => newMockGameDocument(data))
  } as QuerySnapshot;

  const mockGamesQuery = mock(Query);
  when(mockGamesQuery.get()).thenResolve(mockQuerySnapshot);

  const mockGames = mock(CollectionReference);
  when(mockGames.orderBy(anything(), anything())).thenCall(() => {
    console.log('Returning mock query from orderBy()');
    return instance(mockGamesQuery);
  });

  return mockGames;
}

// @ts-ignore
function newMockGameDocument(data: Game): QueryDocumentSnapshot {
  const doc = mock(QueryDocumentSnapshot);
  when(doc.data()).thenCall(() => {
    console.log('Returning document data');
    return data;
  });
  return instance(doc);
}

function mockCollection(): CollectionReference {
  const mockCollection = mock(CollectionReference);
  const mockDocument = mock(DocumentReference);
  when(mockDocument.collection(anyString())).thenReturn(instance(mockCollection));
  when(mockCollection.doc(anyString())).thenReturn(instance(mockDocument));
  return mockCollection;
}

function newMockWriteBatch(): WriteBatch {
  const mockWriteBatch = mock(WriteBatch);
  const mockInstance = instance(mockWriteBatch);
  let setCount = 0;
  console.log('Creating mock write batch');
  when(mockWriteBatch.set(anything(), anything())).thenCall(() => {
    setCount++;
    return mockInstance;
  });
  when(mockWriteBatch.commit()).thenCall(() => {
    console.log('Committing', setCount, 'set operations');
    return Promise.resolve([ mock(WriteResult) ]);
  });
  return mockInstance;
}

main();
