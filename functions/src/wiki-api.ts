import fetch from 'node-fetch';

import { Character, Move } from './wiki-types';

const MOVE_TYPES = [ 'fd0row', 'fd1row', 'fd4row', 'fd5row', 'fd6row' ];

function fetchJson<T = any>(url: string): Promise<T> {
  return fetch(url).then((res: any) => res.json());
}

export class WikiApiService {

  private url: string;

  constructor(url: string) {
    this.url = url.replace(/\/$/, '');
    console.log('WikiApiService: Set API URL to', this.url);
  }

  apiUrl(endpoint: string): string {
    return `${this.url}/${endpoint}/json`;
  }

  getCharacters(game: string): Promise<Character[]> {
    return fetchJson(this.apiUrl(game)).then((characters: Array<any>) =>
      characters.filter(character =>
        'name' in character && 'slug' in character
      ).map((character: any) => {
        return { name: character.name, slug: character.slug };
      })
    );
  }

  private static getAlias(slug: string, game: string): string | undefined {
    if (slug === 'alpha-patroklos') {
      return 'a-patroklos';
    }
    if (slug === 'sophitia' && game === 'sc5') {
      return 'elysium';
    }
    if (slug === 'aeon' && game === 'sc5') {
      return 'lizardman';
    }
    return undefined;
  }

  getMoves(character: Character, gameFrameSlug: string): Promise<Move[]> {
    const alias = WikiApiService.getAlias(character.slug, gameFrameSlug);
    if (alias != null) {
      character.slug = alias;
    }
    const movesUrl = this.apiUrl(`${character.slug}-frame-data-${gameFrameSlug}`);
    return fetchJson(movesUrl).then((moves: Array<any>) =>
      moves.filter(move => 'type' in move && MOVE_TYPES.indexOf(move.type) >= 0)
    );
  }

}
